{% if oncall_enabled is defined and oncall_enabled %}
x-environment: &oncall-environment
  DATABASE_TYPE: sqlite3
  BROKER_TYPE: redis
  BASE_URL: "https://{{ oncall.domain }}"
  SECRET_KEY: "{{ oncall.secret }}"
  REDIS_URI: redis://redis:6379/0
  DJANGO_SETTINGS_MODULE: settings.hobby
  CELERY_WORKER_QUEUE: "default,critical,long,slack,telegram,webhook,retry,celery"
  CELERY_WORKER_CONCURRENCY: "1"
  CELERY_WORKER_MAX_TASKS_PER_CHILD: "100"
  CELERY_WORKER_SHUTDOWN_INTERVAL: "65m"
  CELERY_WORKER_BEAT_ENABLED: "True"
  GRAFANA_API_URL: http://grafana:3000
{% endif %}
services:
{% if oncall_enabled is defined and oncall_enabled %}
  engine:
    image: docker.io/grafana/oncall
    restart: always
    ports:
      - "8080:8080"
    command: >
      sh -c "uwsgi --ini uwsgi.ini"
    environment: *oncall-environment
    volumes:
      - type: bind
        source: /opt/shift-mon/oncall
        target: /var/lib/oncall
    depends_on:
      oncall_db_migration:
        condition: service_completed_successfully
      redis:
        condition: service_healthy

  celery:
    image: docker.io/grafana/oncall
    restart: always
    command: sh -c "./celery_with_exporter.sh"
    environment: *oncall-environment
    volumes:
      - /opt/shift-mon/oncall:/var/lib/oncall
    depends_on:
      oncall_db_migration:
        condition: service_completed_successfully
      redis:
        condition: service_healthy

  oncall_db_migration:
    image: docker.io/grafana/oncall
    command: python manage.py migrate --noinput
    environment: *oncall-environment
    volumes:
      - type: bind
        source: /opt/shift-mon/oncall
        target: /var/lib/oncall
    depends_on:
      redis:
        condition: service_healthy
{% endif %}
  grafana:
    image: docker.io/grafana/grafana-oss:latest
    restart: unless-stopped
    volumes:
      - type: bind
        source: /opt/shift-mon/grafana-config
        target: /etc/grafana
      - type: bind
        source: /opt/shift-mon/grafana
        target: /var/lib/grafana
    depends_on:
      redis:
        condition: service_healthy
{% if oncall_enabled is defined and oncall_enabled %}
    environment:
      GF_INSTALL_PLUGINS: grafana-oncall-app
{% endif %}
  loki:
    image: docker.io/grafana/loki:latest
    restart: unless-stopped
    volumes:
      - type: bind
        source: /opt/shift-mon/loki
        target: /loki
      - type: bind
        source: /opt/shift-mon/loki-config
        target: /etc/loki/
    command:
      - "-config.file=/etc/loki/loki-config.yml"
  victoriametrics:
    restart: unless-stopped
    image: docker.io/victoriametrics/victoria-metrics:stable
    volumes:
      - type: bind
        source: /opt/shift-mon/victoriametrics-storage
        target: /victoriametrics-storage
    command:
      - "--retentionPeriod=90d"
      - "--storageDataPath=/victoriametrics-storage"
      - "--httpListenAddr=:8428"
{% if victoria.insecure is defined and victoria.insecure is true %}
    ports:
      - 8428:8428
{% endif %}
{% if uptime_kuma_enabled is not defined or uptime_kuma_enabled %}
  uptime-kuma:
    image: docker.io/louislam/uptime-kuma:1
    restart: always
    volumes:
      - type: bind
        source: /opt/shift-mon/uptime-kuma
        target: /app/data
{% endif %}
  crowdsec:
    image: docker.io/crowdsecurity/crowdsec:latest
    restart: always
    environment:
      COLLECTIONS: "crowdsecurity/traefik"
    depends_on:
     - 'traefik'
    volumes:
      - type: bind
        source: /opt/shift-mon/crowdsec-db
        target: /var/lib/crowdsec/data/
      - type: bind
        source: /opt/shift-mon/crowdsec-config
        target: /etc/crowdsec/
      - type: bind
        source: /opt/shift-mon/traefik
        target: /etc/traefik
      - type: bind
        source: /opt/shift-mon/crowdsec-log
        target: /var/log
{% if crowdsec_api_key is defined %}
  crowdsec-bouncer:
    image: docker.io/fbonalair/traefik-crowdsec-bouncer:latest
    restart: unless-stopped
    depends_on: 
      - crowdsec
    environment:
      CROWDSEC_BOUNCER_API_KEY: "{{ crowdsec_api_key }}"
      CROWDSEC_AGENT_HOST: "crowdsec:8080"
      GIN_MODE: "release"
{% endif %}
  redis:
    image: docker.io/redis:latest
    restart: always
    expose:
      - 6379
    volumes:
      - type: bind
        source: /opt/shift-mon/redis
        target: /data
    deploy:
      resources:
        limits:
          memory: 500m
          cpus: "0.5"
    healthcheck:
      test: ["CMD", "redis-cli", "ping"]
      timeout: 5s
      interval: 5s
      retries: 10
  traefik:
    restart: unless-stopped
    image: docker.io/traefik:latest
    volumes:
      - type: bind
        source: /opt/shift-mon/traefik
        target: /etc/traefik
    ports:
     - 80:80
     - 443:443
{% if dns.provider is defined and dns.auth_values is defined %}
    environment:
{% for key, value in dns.auth_values.items() %}
      {{key}}: '{{value}}'
{% endfor %}
{% endif %}
    command:
      - "--auth.config=/config/auth-config.yml"